	<?php
    $listeVilles = $villeManager->recupererToutesVilles();
    $nbVilles = count($listeVilles);
  ?>

  <h2>Liste des villes</h2>

  <p>Actuellement <?php echo $nbVilles ?> villes sont enregistrées</p>

  <?php if(!empty($listeVilles)){ ?>

    <table>

      <!-- Entête -->
      <thead>

        <!-- Ligne -->
        <tr>
          <!-- Colonne d'entête -->
          <th>Numéro</th>
          <th>Nom</th>
        </tr>
      </thead>

      <?php foreach ($listeVilles as $ville) { ?>
        <tr>
          <td><?php echo $ville->getNumVille(); ?></td>
          <td><?php echo $ville->getNomVille(); ?></td>
        </tr>
      <?php } ?>

    </table>

  <?php } ?>
