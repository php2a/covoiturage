	<?php
    $listeParcours = $parcoursManager->recupererTousParcours();
    $nbParcours = count($listeParcours);
  ?>

  <h2>Liste des parcours</h2>

  <p>Actuellement <?php echo $nbParcours ?> parcours sont enregistrés</p>

  <?php if(!empty($listeParcours)){ ?>

    <table>

      <!-- Entête -->
      <thead>

        <!-- Ligne -->
        <tr>
          <!-- Colonne d'entête -->
          <th>Numéro</th>
          <th>Nom ville départ</th>
          <th>Nom ville arrivée</th>
          <th>Nombre de kilomètres</th>
        </tr>
      </thead>

      <?php foreach ($listeParcours as $parcours) {
        ?>
        <tr>
          <td><?php echo $parcours->getNumParcours(); ?></td>
          <td><?php echo $villeManager->recupererNomVilleParNum($parcours->getNumVille1Parcours()); ?></td>
          <td><?php echo $villeManager->recupererNomVilleParNum($parcours->getNumVille2Parcours()); ?></td>
          <td><?php echo $parcours->getKmParcours(); ?></td>
        </tr>
      <?php } ?>

    </table>

  <?php } ?>
