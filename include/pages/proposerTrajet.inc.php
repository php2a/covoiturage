	<h2>Proposer un trajet</h2>

  <?php
  $listeVilles = $parcoursManager->recupererToutesVillesDesParcours();

	$premierePageRemplie = 	!empty($_POST['villeDepart']) && $_POST['villeDepart']!="Choisissez";
	$deuxiemePageRemplie = 	!empty($_POST['villeArrivee']) && $_POST['villeArrivee']!="Choisissez" && !empty($_POST['dateDepart']) &&
													!empty($_POST['heureDepart']) && !empty($_POST['nbPlaces']);

	if(!$premierePageRemplie && !$deuxiemePageRemplie){
	?>

    <form action=# method="post">
      <label>Ville de départ :</label>

      <br>

      <select name="villeDepart">
        <option value="Choisissez">-- Choisissez --</option>
        <?php foreach ($listeVilles as $ville) { ?>
            <option value="<?php echo $ville->getNumVille(); ?>"><?php echo $ville->getNomVille();?></option>
        <?php } ?>
      </select>

      <br>

      <button type="submit" value="Valider">Valider</button>
    </form>

  <?php
} else if($premierePageRemplie && !$deuxiemePageRemplie){

		$_SESSION['villeDepart'] = $_POST['villeDepart'];

  ?>

  <form action=# method="post">

		<div>
    	<div>
				<label>Ville de départ :</label>
				<p><?php echo $villeManager->recupererNomVilleParNum($_POST['villeDepart']); ?></p>
			</div>

			<div>
    		<label>Ville d'arrivée :</label>

		    <select name="villeArrivee">
		      <option value="Choisissez">-- Choisissez --</option>
		      <?php
					$listeVillesArriveePossible = $parcoursManager->recupererVillesArriveePossibleViaIdVilleDepart($_POST['villeDepart']);

					foreach ($listeVillesArriveePossible as $ville) { ?>
		          <option value="<?php echo $ville->getNumVille(); ?>"><?php echo $ville->getNomVille();?></option>
		      <?php } ?>
		    </select>
			</div>
		</div>

		<div class="clear"></div>

    <div>
			<div>
		    <label>Date de départ :</label>
		    <input name="dateDepart" type="date" value="<?php echo date("Y-m-d"); ?>">
			</div>

			<div>
		    <label>Heure de départ :</label>
	    	<input name="heureDepart" type="time" value="<?php echo date("H:i"); ?>">
			</div>
    </div>

		<div class="clear"></div>

		<div>
			<div>
    		<label>Nombre de places :</label>
				<input name="nbPlaces" type="number">
			</div>
		</div>

		<div class="clear"></div>

    <button type="submit" value="Valider">Valider</button>
  </form>

  <?php
	} else {

		$paramsParcours =
			array(
				'vil_num1' => $_SESSION['villeDepart'],
				'vil_num2' => $_POST['villeArrivee']
			);

		$parcoursPropose = $parcoursManager->creerParcoursDepuisTableau($paramsParcours);

		$parcours = $parcoursManager->parcoursExisteDeja($parcoursPropose);

		if($parcours){

			$sens = $trajetManager->getSensTrajet($parcours->getNumParcours(), $_SESSION['villeDepart']);

			$trajetParams =
				array(
					'par_num' => $parcours->getNumParcours(),
					'per_num' => $_SESSION['numeroPersonneConnecte'],
					'pro_date' => $_POST['dateDepart'],
					'pro_time' => $_POST['heureDepart'],
					'pro_place' => $_POST['nbPlaces'],
					'pro_sens' => $sens
				);

			$nouveauTrajet = $trajetManager->creerTrajetDepuisTableau($trajetParams);

			$ajout = $trajetManager->ajouterTrajet($nouveauTrajet);

			unset($_SESSION['villeDepart']);

			if($ajout){
			?>
				<div>
					<img src="image/valid.png" alt="icon valider">
					<p>Le trajet a été ajouté</p>
				</div>
				<input type="button" value="Continuer" onclick="location.href='index.php?page=9';" />

			<?php
			} else {
			?>
				<div>
					<img src="image/erreur.png" alt="icon erreur">
					<p>Erreur : le trajet n'a pas été ajouté</p>
				</div>
				<input type="button" value="Réessayer" onclick="location.href='index.php?page=9';" />
			<?php
			}

		}
	}
?>
