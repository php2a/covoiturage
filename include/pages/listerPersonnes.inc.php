	<?php
    $listePersonnes = $personneManager->recupererToutesPersonnes();
    $nbPeronnes = count($listePersonnes);
  ?>

  <h2>Liste des personnes enregistrées</h2>

  <p>Actuellement <?php echo $nbPeronnes ?> personnes sont enregistrées</p>

  <?php if(!empty($listePersonnes)){ ?>

    <table>

      <!-- Entête -->
      <thead>

        <!-- Ligne -->
        <tr>
          <!-- Colonne d'entête -->
          <th>Numéro</th>
          <th>Nom</th>
          <th>Prénom</th>
        </tr>
      </thead>

      <?php foreach ($listePersonnes as $personne) {
        ?>
        <tr>
          <td><a href="index.php?page=13&numPersonneRecherchee=<?php echo $personne->getNumPersonne(); ?>"><?php echo $personne->getNumPersonne(); ?></a></td>
          <td><?php echo $personne->getNomPersonne(); ?></td>
          <td><?php echo $personne->getPrenomPersonne(); ?></td>
        </tr>
      <?php } ?>

    </table>

  <?php } ?>
