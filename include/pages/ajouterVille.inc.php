  <h2>Ajouter une ville</h2>

  <?php
	if(empty($_POST['nomVille'])){
	?>

    <form action=# method="post">
      <label>Nom :</label>

  		<input type="text" name="nomVille" size="20"/>

      <button type="submit" value="Valider">Valider</button>
    </form>

  <?php
	} else {

    $paramsVille = array(
      'vil_nom' => $_POST['nomVille']
    );

    $nouvelleVille = $villeManager->creerVilleDepuisTableau($paramsVille);

    $ajout = $villeManager->verifierSiVilleExisteDeja($nouvelleVille);

    if($ajout){

  		$ajout = $villeManager->ajouterVille($nouvelleVille);

  		if($ajout){
  	?>
        <div>
          <img src="image/valid.png" alt="icon valider">
          <p>La ville "<b><?php echo $_POST['nomVille'] ?></b>" a été ajoutée</p>
        </div>
        <input type="button" value="Continuer" onclick="location.href='index.php?page=7';" />
    <?php
  		} else {
  	?>
        <div>
          <img src="image/erreur.png" alt="icon erreur">
          <p>Erreur : La ville "<b><?php echo $_POST['nomVille'] ?></b>" n'a pas été ajoutée</p>
        </div>
      <input type="button" value="Réessayer" onclick="location.href='index.php?page=7';" />
  	<?php
  		}
    } else {
      ?>
          <div>
            <img src="image/erreur.png" alt="icon erreur">
            <p>La ville "<b><?php echo $_POST['nomVille'] ?></b>" existe déjà</p>
          </div>
        <input type="button" value="Réessayer" onclick="location.href='index.php?page=7';" />
      <?php
    }
	}
	?>
