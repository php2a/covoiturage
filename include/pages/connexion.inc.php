<h2>Pour vous connecter</h2>

<?php
  if(empty($_POST['nomUtilisateur']) || empty($_POST['motDePasse']) || empty($_POST['reponseCaptcha'])) {

    $numImage1 = rand(1,9);
    $numImage2 = rand(1,9);
?>

  <form action="" method="post">
    <label>Nom d'utilisateur :</label>

		<br>

    <input type="text" name="nomUtilisateur" size="20"/>

		<br>

    <label>Mot de passe :</label>

		<br>

    <input type="password" name="motDePasse" size="20"/>

		<br>

    <input type="hidden" value="<?php echo $numImage1 ?>" name="numImage1"/>
    <input type="hidden" value="<?php echo $numImage2 ?>" name="numImage2"/>

    <label>
      <img src="image/nb/<?php echo $numImage1 ?>.jpg" alt="captcha image "<?php echo $numImage1 ?>>
      +
      <img src="image/nb/<?php echo $numImage2 ?>.jpg" alt="captcha image "<?php echo $numImage2 ?>>
      =
    </label>

		<br>

    <input type="number" name="reponseCaptcha" size="20"/>

		<br>

    <button type="submit" value="Valider">Valider</button>
  </form>

<?php
  } else {

    if(($_POST['numImage1'] + $_POST['numImage2']) ==  $_POST['reponseCaptcha']){

      $personneConnecte = $connexionManager->tentativeConnexion($_POST['nomUtilisateur'], $_POST['motDePasse']);

      if (!empty($personneConnecte)) {

        $_SESSION['numeroPersonneConnecte'] = $personneConnecte->getNumPersonne();
        $_SESSION['loginPersonneConnecte'] = $personneConnecte->getLoginPersonne();

        header('Location: index.php');
        exit();

      } else {
?>
      <div>
        <img src="image/erreur.png" alt ="icon erreur">
        <p>Compte non reconnu.</p>
      </div>
      <input type="button" value="Réessayer" onclick="location.href='index.php?page=11';" />
<?php
      }
    } else {
?>
      <div>
        <img src="image/erreur.png" alt ="icon erreur">
        <p>captcha incorrect.</p>
      </div>
      <input type="button" value="Réessayer" onclick="location.href='index.php?page=11';" />
<?php
    }
  }
?>
