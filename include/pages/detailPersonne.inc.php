<?php

  $personne = $personneManager->recupererPersonneParId($_GET['numPersonneRecherchee']);

  $estEtudiant = $personneManager -> estEtudiant($_GET['numPersonneRecherchee']);

  if($estEtudiant){
    $message = "l'étudiant";
    $etudiant = $etudiantManager->recupererEtudiantViaNumero($_GET['numPersonneRecherchee']);
  } else {
    $message = "le salarié";
    $salarie = $salarieManager->recupererSalarieViaNumero($_GET['numPersonneRecherchee']);
  }

?>
<h2>Détail sur <?php echo $message." ".$personne->getNomPersonne(); ?> </h2>

<table>

  <!-- Entête -->
  <thead>

    <!-- Ligne -->
    <tr>
      <!-- Colonne d'entête -->
      <th>Prénom</th>
      <th>Mail</th>
      <th>Tel</th>

      <?php if($estEtudiant){?>

        <th>Département</th>
        <th>Ville</th>

      <?php } else {?>

        <th>Tel Pro</th>
        <th>Fonction</th>

      <?php }?>
    </tr>
  </thead>

  <tr>
    <td><?php echo $personne->getPrenomPersonne(); ?></td>
    <td><?php echo $personne->getMailPersonne(); ?></td>
    <td><?php echo $personne->getTelPersonne(); ?></td>

    <?php if($estEtudiant){?>
      <td><?php echo $etudiant->getDepartementEtudiant(); ?></td>
      <?php
        $numDepEtudiant = $etudiant->getDepartementEtudiant();
        $numVilleDepatementEtudiant = $villeManager->recupererVilleNumViaNumDepartement($numDepEtudiant);
        $villeDepatementEtudiant = $villeManager->recupererVilleParNum($numVilleDepatementEtudiant);
      ?>
      <td><?php echo $villeDepatementEtudiant->getNomVille(); ?></td>
    <?php } else {?>
      <td><?php echo $salarie->getTelProfessionnelSalarie(); ?></td>
      <td><?php echo $fonctionManager->recupererFonctionSalarieViaNumero($salarie->getFonctionSalarie())->getLibelleFonction(); ?></td>
    <?php }?>
  </tr>

</table>
