	<?php

	//Fonction permettant de réaliser les action suivante :
	//Récupérér les données du tableau $_POST, d'en faire une objet Personne et d'ajouter cette nouvelle personne
	function procedureAjoutPersonne($personneManager,$connexionManager){

		$tableauTemp = array(
			'per_nom' => $_POST['nomNouvellePersonne'],
			'per_prenom' => $_POST['prenomNouvellePersonne'],
			'per_tel' => $_POST['telNouvellePersonne'],
			'per_mail' => $_POST['mailNouvellePersonne'],
			'per_login' => $_POST['loginNouvellePersonne'],
			'per_pwd' => $connexionManager->encryptPwd($_POST['mdpNouvellePersonne'])
		);

		if($GLOBALS['modeEdit']){
			$tableauTemp['per_num'] = $_GET['idPersonneAModifier'];
		}

		$nouvellePersonne = $personneManager->creerPersonneDepuisTableau($tableauTemp);

		if($GLOBALS['modeEdit']){
			$ajouterPersonne = $personneManager->updatePersonne($nouvellePersonne);
		} else {
			$ajouterPersonne = $personneManager->ajouterPersonne($nouvellePersonne);
		}

		if($ajouterPersonne){
			return $nouvellePersonne;
		} else {
			return NULL;
		}
	}

	if(isset($_GET['idPersonneAModifier'])){
		$GLOBALS['modeEdit'] = TRUE;
		$personneAModifier = $personneManager ->recupererPersonneParId($_GET['idPersonneAModifier']);
	} else {
		$GLOBALS['modeEdit'] = FALSE;
	}
	?>

	<?php
	$pasAjoutPersonneEnCours = 	(	empty($_POST['nomNouvellePersonne']) ||
	empty($_POST['prenomNouvellePersonne']) ||
	empty($_POST['telNouvellePersonne']) ||
	empty($_POST['mailNouvellePersonne']) ||
	empty($_POST['loginNouvellePersonne']) ||
	empty($_POST['mdpNouvellePersonne']) ||
	empty($_POST['typePersonne'])
);

$pasAjoutEtudiantEnCours =  ( empty($_POST['choixAnnneeNouvellePersonne']) ||
empty($_POST['choixDepartementNouvellePersonne'])
);

$pasAjoutSalarieEnCours =  	(	empty($_POST['choixTelProNouvellePersonne']) ||
empty($_POST['choixFonctionNouvellePersonne'])
);

//S'il y a pas d'ajout de personne en cours
if($pasAjoutPersonneEnCours && $pasAjoutEtudiantEnCours && $pasAjoutSalarieEnCours){

	if($GLOBALS['modeEdit']){ ?>
		<h2>Modifier une personne</h2>
	<?php } else { ?>
		<h2>Ajouter une personne</h2>
	<?php } ?>

	<form action="#" method="post" onsubmit="return verifFormulaireAjoutPersonne(this)">

		<div>
			<div>
				<label>Nom :</label>
				<input type="text" name="nomNouvellePersonne" size="20"
				<?php if($GLOBALS['modeEdit']){echo "value=".$personneAModifier->getNomPersonne();} ?> required/>
			</div>

			<div>
				<label>Prenom :</label>
				<input type="text" name="prenomNouvellePersonne" size="20"
				<?php if($GLOBALS['modeEdit']){echo "value=".$personneAModifier->getPrenomPersonne();} ?> required/>
			</div>
		</div>

		<div class="clear"></div>

		<div>
			<div>
				<label>Téléphone :</label>
				<input type="number" name="telNouvellePersonne" size="20" onblur="verifTel(this)"
				<?php if($GLOBALS['modeEdit']){echo "value=".$personneAModifier->getTelPersonne();} ?> required/>
			</div>

			<div>
				<label>Mail :</label>
				<input type="email" name="mailNouvellePersonne" size="20"
				<?php if($GLOBALS['modeEdit']){echo "value=".$personneAModifier->getMailPersonne();} ?> required/>
			</div>
		</div>

		<div class="clear"></div>

		<div>
			<div>
				<label>Login :</label>
				<input type="text" name="loginNouvellePersonne" size="20"
				<?php if($GLOBALS['modeEdit']){echo "value=".$personneAModifier->getLoginPersonne();} ?> required/>
			</div>

			<div>
				<label>Mot de passe :</label>
				<input type="password" name="mdpNouvellePersonne" size="20" required/>
			</div>
		</div>

		<div class="clear"></div>

		<div class="choixCategorie">
			<label>Catégorie :</label>

			<input type="radio" name="typePersonne" value="etudiant"
			<?php if($GLOBALS['modeEdit'] && $personneManager->estEtudiant($personneAModifier->getNumPersonne())){ echo 'checked="checked"'; } ?> required/>
			<span>Etudiant</span>
			<input type="radio" name="typePersonne" value="personnel"
			<?php if($GLOBALS['modeEdit'] && !$personneManager->estEtudiant($personneAModifier->getNumPersonne())){ echo 'checked="checked"'; } ?> required/>
			<span>Personnel</span>
		</div>

		<div class="clear"></div>

		<input type="hidden" name="idPersonneAModifier" <?php if(isset($_GET['idPersonneAModifier'])){ echo "value=".$_GET['idPersonneAModifier']; } ?> />

		<button type="submit" value="Valider">
			<?php if($GLOBALS['modeEdit']){?>
				Modifier
				<?php
			} else {
				?>
				Valider
				<?php
			}
			?>
		</button>
	</form>

	<?php
}
//S'il y a un ajout de personne étudiante en cours
else if (	!$pasAjoutPersonneEnCours &&
$pasAjoutEtudiantEnCours &&
$pasAjoutSalarieEnCours &&
$_POST['typePersonne']=="etudiant"
){

	$nouvellePersonne = procedureAjoutPersonne($personneManager,$connexionManager);

	if($nouvellePersonne){
		$listeDivisions = $divisionManager->recupererToutesDivisions();
		$listeDepartements = $departementManager->recupererTousDepartement();

		if($GLOBALS['modeEdit']){ ?>
			<h2>Modifier un étudiant</h2>
		<?php } else { ?>
			<h2>Ajouter un étudiant</h2>
		<?php } ?>

		<form action="" method="post">
			<label>Année :</label>
			<select name="choixAnnneeNouvellePersonne">
				<?php foreach ($listeDivisions as $division) { ?>
					<option value="<?php echo $division->getNumDivision(); ?>">
						<?php echo $division->getNomDivision(); ?>
					</option>
				<?php } ?>
			</select>

			<label>Département :</label>
			<select name="choixDepartementNouvellePersonne">
				<?php foreach ($listeDepartements as $departement) { ?>
					<option value="<?php echo $departement->getNumDepartement(); ?>">
						<?php echo $departement->getNomDepartement(); ?>
					</option>
				<?php } ?>
			</select>

			<button type="submit" value="Valider">Valider</button>
		</form>
		<?php
	}
}
//S'il y a un ajout de personne salarié en cours
else if (	!$pasAjoutPersonneEnCours &&
$pasAjoutEtudiantEnCours &&
$pasAjoutSalarieEnCours &&
$_POST['typePersonne']=="personnel"
){

	$nouvellePersonne = procedureAjoutPersonne($personneManager,$connexionManager);
	$listeFonctions = $fonctionManager->recupererToutesFonctions();

	if($GLOBALS['modeEdit']){ ?>
		<h2>Modifier un salarié</h2>
	<?php } else { ?>
		<h2>Ajouter un salarié</h2>
	<?php } ?>

	<form action=# method="post" onsubmit="return verifFormulaireAjoutPersonne(this)">
		<label>Téléphone professionnel :</label>
		<input type="number" name="telNouvellePersonne" size="20" onblur="verifTel(this)"/>

		<label>Fonction :</label>
		<select name="choixFonctionNouvellePersonne">
			<?php foreach ($listeFonctions as $fonction) { ?>
				<option value="<?php echo $fonction->getNumFonction(); ?>">
					<?php echo $fonction->getLibelleFonction(); ?>
				</option>
			<?php } ?>
		</select>

		<button type="submit" value="Valider">Valider</button>
	</form>
	<?php
}

//Si il y a un ajout de personne étudiante finalisée
else if (!$pasAjoutEtudiantEnCours){

	$tableauTemp = array(
		'div_num' => $_POST['choixAnnneeNouvellePersonne'],
		'dep_num' => $_POST['choixDepartementNouvellePersonne']
	);

	if($GLOBALS['modeEdit']){
		$tableauTemp['per_num'] = $_GET['idPersonneAModifier'];
	} else {
		$tableauTemp['per_num'] = $_SESSION['idPersonneEnCreation'];
	}

	$nouvelEtudiant = $etudiantManager->creerEtudiantDepuisTableau($tableauTemp);

	if($GLOBALS['modeEdit']){
		if($personneManager->estEtudiant($_GET['idPersonneAModifier'])){
			$ajoutEtudiant = $etudiantManager->updateEtudiant($nouvelEtudiant);
		} else {
			$ajoutEtudiant = $etudiantManager->ajouterEtudiant($nouvelEtudiant);
			$salarieManager->supprimerSalarie($_GET['idPersonneAModifier']);
		}
	} else {
		$ajoutEtudiant = $etudiantManager->ajouterEtudiant($nouvelEtudiant);
	}

	if($ajoutEtudiant){
		?>
		<div>
			<img src="image/valid.png" alt="icon valider">
			<p>La personne a bien été ajoutée</p>
		</div>
		<input type="button" value="Continuer" onclick="location.href='index.php?page=1';" />
		<?php
	} else {
		?>
		<div>
			<img src="image/erreur.png" alt="icon erreur">
			<p>Erreur : La personne n'a pas été ajoutée</p>
		</div>
		<input type="button" value="Continuer" onclick="location.href='index.php?page=1';" />
		<?php
	}
}

//Si il y a un ajout de personne salarié finalisée
else if	(!$pasAjoutSalarieEnCours){

	$tableauTemp = array(
		'sal_telprof' => $_POST['telNouvellePersonne'],
		'fon_num' => $_POST['choixFonctionNouvellePersonne']
	);

	if($GLOBALS['modeEdit']){
		$tableauTemp['per_num'] = $_GET['idPersonneAModifier'];
	} else {
		$tableauTemp['per_num'] = $_SESSION['idPersonneEnCreation'];
	}

	$nouveauSalarie = $salarieManager->creerSalarieDepuisTableau($tableauTemp);

	if($GLOBALS['modeEdit']){
		if(!$personneManager->estEtudiant($_GET['idPersonneAModifier'])){
			$ajoutSalarie = $salarieManager->updateSalarie($nouveauSalarie);
		} else {
			$ajoutSalarie = $salarieManager->ajouterSalarie($nouveauSalarie);
			$etudiantManager->supprimerEtudiant($_GET['idPersonneAModifier']);
		}
	} else {
		$ajoutSalarie = $salarieManager->ajouterSalarie($nouveauSalarie);
	}

	if($ajoutSalarie){
		?>
		<div>
			<img src="image/valid.png" alt="icon valider">
			<p>La personne a bien été <?php if($GLOBALS['modeEdit']){echo "modifié";}else{echo "ajoutée";} ?></p>
		</div>
		<input type="button" value="Continuer" onclick="location.href='index.php?page=1';" />
		<?php
	} else {
		?>
		<div>
			<img src="image/erreur.png" alt="icon erreur">
			<p>Erreur : La personne n'a pas été <?php if($GLOBALS['modeEdit']){echo "modifié";}else{echo "ajoutée";} ?></p>
		</div>
		<input type="button" value="Réessayer" onclick="location.href='index.php?page=1';" />
		<?php
	}
}
?>
