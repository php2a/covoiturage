	<?php

  $listeVilles = $villeManager->recupererToutesVilles();
	?>

  <h2>Ajouter un parcours</h2>


  <?php
	if(empty($_POST['ville1Parcours']) || empty($_POST['ville2Parcours']) || empty($_POST['nbKmParcours'])){
	?>

    <form action="#" method="post" onsubmit="return validateFormulaireParcours(this)">
      <label>Ville 1 :</label>

  		<select id="ville1Parcours" name="ville1Parcours">
        <?php foreach ($listeVilles as $ville) { ?>
            <option value="<?php echo $ville->getNumVille(); ?>"><?php echo $ville->getNomVille();?></option>
        <?php } ?>
      </select>

      <label>Ville 2 :</label>

      <select id="ville2Parcours" name="ville2Parcours">
        <?php foreach ($listeVilles as $ville) { ?>
            <option value="<?php echo $ville->getNumVille(); ?>"><?php echo $ville->getNomVille();?></option>
        <?php } ?>
      </select>

      <label>Nombre de kilomètre(s) :</label>

  		<input type="number" name="nbKmParcours" size="20"/>

      <br>

      <button type="submit" value="Valider">Valider</button>

    </form>

  <?php
	} else {
			$paramsParcours =
	      array(
	        'vil_num1' => $_POST['ville1Parcours'],
	        'vil_num2' => $_POST['ville2Parcours'],
	        'par_km' => $_POST['nbKmParcours']
	      );

	    $nouveauParcours = $parcoursManager->creerParcoursDepuisTableau($paramsParcours);

			$parcours = $parcoursManager->parcoursExisteDeja($nouveauParcours);

			if($parcours == NULL){
	    	$ajout = $parcoursManager->ajouterParcours($nouveauParcours);

				if($ajout){
		?>
	    		<img src="image/valid.png" alt="icon valider">
	    		<p>Le parcours a été ajouté</p>
	  <?php
				} else {
		?>
					<div>
						<img src="image/erreur.png" alt="icon erreur">
						<p>Erreur :le parcours n'a pas été ajouté</p>
					</div>
					<input type="button" value="Réessayer" onclick="location.href='index.php?page=5';" />

		<?php
				}
			} else {
		?>
					<div>
						<img src="image/erreur.png" alt="icon erreur">
						<p>Ce parcours existe déjà</p>
					</div>
					<input type="button" value="Réessayer" onclick="location.href='index.php?page=5';" />

	<?php
		}
}
