<?php
  $listePersonnes = $personneManager->recupererToutesPersonnes();
  $nbPeronnes = count($listePersonnes);

  if(empty($_GET['numPersonneASupprimer'])){
?>

<h2>Liste des personnes enregistrées</h2>

<p>Actuellement <?php echo $nbPeronnes ?> personnes sont enregistrées</p>

<?php if(!empty($listePersonnes)){ ?>

  <table>

    <!-- Entête -->
    <thead>

      <!-- Ligne -->
      <tr>
        <!-- Colonne d'entête -->
        <th>Numéro</th>
        <th>Nom</th>
        <th>Prénom</th>
        <th>Supprimer</th>
      </tr>
    </thead>

    <?php foreach ($listePersonnes as $personne) {
      ?>
      <tr>
        <td><?php echo $personne->getNumPersonne(); ?></td>
        <td><?php echo $personne->getNomPersonne(); ?></td>
        <td><?php echo $personne->getPrenomPersonne(); ?></td>
        <td><a href="index.php?page=4&numPersonneASupprimer=<?php echo $personne->getNumPersonne(); ?>"><img src="image/erreur.png" alt="icon erreur"></a></td>
      </tr>
    <?php } ?>

  </table>

<?php
    }
  } else {

    $personne = $personneManager->recupererPersonneParId($_GET['numPersonneASupprimer']);
		$suppr = $personneManager->supprimerPersonne($_GET['numPersonneASupprimer']);

		if($suppr){
?>
      <div>
    	   <img src="image/valid.png" alt="icon valider">
    	   <p>La personne "<b><?php echo $personne->getNomPersonne()." ".$personne->getPrenomPersonne() ?></b>" a bien été supprimée</p>
      </div>
      <input type="button" value="Continuer" onclick="location.href='index.php?page=4';" />
<?php
		} else {
?>
      <div>
        <img src="image/erreur.png" alt="icon erreur">
        <p>Erreur : La personne "<b><?php echo $personne->getNomPersonne()." ".$personne->getPrenomPersonne() ?></b>" n'a pas été supprimée</p>
      </div>
      <input type="button" value="Réessayer" onclick="location.href='index.php?page=4';" />
<?php
		}
	}

?>
