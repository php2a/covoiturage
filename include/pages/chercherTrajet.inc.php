<h2>Rechercher un trajet</h2>

<?php

$listeVillesDepartTrajet = $trajetManager->recupererToutesVillesDepartDesTrajet();

$premierePageRemplie =  !empty($_POST['villeDepart']) && $_POST['villeDepart'] != "Choisissez";

$deuxiemePageRemplie =  !empty($_POST['villeArrivee']) && !empty($_POST['dateDepart']) &&
                          ( !empty($_POST['precision']) && $_POST['precision']!="Choisissez"  ) &&
                          ( !empty($_POST['heureDepart']) && $_POST['heureDepart']!="Choisissez"  )
                        ;

if(!$premierePageRemplie && !$deuxiemePageRemplie){
?>

  <form action=# method="post">
    <label>Ville de départ :</label>

    <br>

    <select name="villeDepart">
      <option value="Choisissez">-- Choisissez --</option>
      <?php foreach ($listeVillesDepartTrajet as $villeDepartTrajet) { ?>
          <option value="<?php echo $villeDepartTrajet->getNumVille(); ?>"><?php echo $villeDepartTrajet->getNomVille();?></option>
      <?php } ?>
    </select>

    <br>

    <button type="submit" value="Valider">Valider</button>
  </form>

<?php
} else if($premierePageRemplie && !$deuxiemePageRemplie){

  $_SESSION['villeDepart'] = $_POST['villeDepart'];

  $listeVillesArriveePossible = $trajetManager->recupererVillesArriveePossibleViaIdVilleDepart($_SESSION['villeDepart']);

?>

<form action=# method="post">

  <div>
    <div>
      <label>Ville de départ :</label>
      <label> <?php echo $villeManager->recupererNomVilleParNum($_SESSION['villeDepart']); ?></label>
    </div>

    <div>
      <label>Ville d'arrivée :</label>
      <select name="villeArrivee">
        <option value="Choisissez">-- Choisissez --</option>
        <?php foreach ($listeVillesArriveePossible as $villeArrivee) { ?>
            <option value="<?php echo $villeArrivee->getNumVille(); ?>"><?php echo $villeArrivee->getNomVille();?></option>
        <?php } ?>
      </select>
    </div>
  </div>

  <div class="clear"></div>

  <div>
    <div>
        <label>Date de départ :</label>
        <input name="dateDepart" type="date" value="<?php echo date("Y-m-d"); ?>">
    </div>

    <div>
      <label>Précision :</label>

      <select name="precision">
        <option value="Choisissez">-- Choisissez --</option>
        <option value="1">Ce jour</option>
        <option value="2">+/- 1 jour</option>
        <option value="3">+/- 2 jour</option>
        <option value="4">+/- 3 jour</option>
      </select>
    </div>
  </div>

  <div class="clear"></div>

  <div>
    <div>
      <label>A partir de :</label>

      <select name="heureDepart">
        <option value="Choisissez">-- Choisissez --</option>

        <?php for($i=0; $i<24; $i++) {
          if($i<10){ ?>
            <option value="0<?php echo $i ?>:00:00"><?php echo $i."h"; ?></option>
        <?php
          }else { ?>
            <option value="<?php echo $i ?>:00:00"><?php echo $i."h"; ?></option>
        <?php
          }
         } ?>
      </select>
    </div>
  </div>

  <div class="clear"></div>

  <button type="submit" value="Valider">Valider</button>

</form>

<?php
  } else {

    $paramsParcours =
      array(
        'vil_num1' => $_SESSION['villeDepart'],
        'vil_num2' => $_POST['villeArrivee']
      );

    $parcoursRecherche = $parcoursManager->creerParcoursDepuisTableau($paramsParcours);

    $parcours = $parcoursManager->parcoursExisteDeja($parcoursRecherche);

    $sensTrajet = $trajetManager->getSensTrajet($parcours->getNumParcours(),$_SESSION['villeDepart']);

    $listeTrajets = $trajetManager->recupererTrajetPossibleAvecParametres(
      $parcours->getNumParcours(),$sensTrajet,$_POST['dateDepart'],$_POST['heureDepart'],$_POST['precision']
    );

    if(!empty($listeTrajets)){
?>

      <table>

        <thead>
          <tr>
            <th>Ville départ</th>
            <th>Ville Arrivé</th>
            <th>Date départ</th>
            <th>Heure départ</th>
            <th>Nombre de place(s)</th>
            <th>Nom du covoitureur</th>
          </tr>
        </thead>

        <?php foreach ($listeTrajets as $trajet) {
            $idPersonne = $trajet->getNumPersonneTrajet();
          ?>
          <tr>
            <td><?php echo $villeManager->recupererNomVilleParNum($_SESSION['villeDepart']); ?></td>
            <td><?php echo $villeManager->recupererNomVilleParNum($_POST['villeArrivee']); ?></td>
            <td><?php echo $trajet->getDateTrajet(); ?></td>
            <td><?php echo $trajet->getTimeTrajet(); ?></td>
            <td><?php echo $trajet->getPlaceTrajet(); ?></td>
            <td class="showPersonneAvis"><?php echo $personneManager->recupererPersonneParId($idPersonne)->getNomPrenomPersonne(); ?>
              <div class="popUpAvis">
                <div class="trianglePopUpAvis"></div>
                <p class="textPopUpAvis"><?php echo $avisManager->recupererAppreciationViaIdPersonne($idPersonne); ?></p>
              </div>
            </td>

          </tr>
        <?php }
        unset($_SESSION['villeDepart']);
        ?>

      </table>

      <input type="button" value="Rechercher un autre trajet" onclick="location.href='index.php?page=10';" />

<?php
} else {
?>
  <div>
    <img src="image/erreur.png" alt="icon erreur">
    <p>Désole, pas de trajet disponible !</p>
  </div>
  <input type="button" value="Réessayer" onclick="location.href='index.php?page=10';" />

<?php
  }
}
?>
