<?php
class AvisManager{

  //Conctructeur
	public function __construct($db){
		$this->db = $db;
	}


	//Fonction permettant de récupérer un avis structuré sur une personne
	public function recupererAppreciationViaIdPersonne($idPersonne){
		return "Moyenne avis : ".$this->recupererMoyeneneAvisViaIdPersonne($idPersonne)
					." Dernier avis : ".$this->recupererDernierAvisViaIdPersonne($idPersonne);
	}

	//Fonction qui nous renvoie la moyenne des avis sur une personne
	public function recupererMoyeneneAvisViaIdPersonne($idPersonne){

		if(!is_null($idPersonne)){
			$req = $this->db->prepare(
				"SELECT ROUND(AVG(avi_note),1) AS moyenne FROM avis WHERE per_num = :per_num"
      );

			$req->bindValue(':per_num',$idPersonne,PDO::PARAM_STR);

			$req->execute();

			$avis = $req->fetch(PDO::FETCH_ASSOC);

			$moyAvis = $avis['moyenne'];

			return $moyAvis;

			$req->closeCursor();
		}
	}

	//Fonction permettant de récupérer le dernier avis émit su une personne
	public function recupererDernierAvisViaIdPersonne($idPersonne){
		if(!is_null($idPersonne)){
			$req = $this->db->prepare(
				"SELECT avi_comm AS dernierAvis FROM avis WHERE per_num = :per_num"
      );

			$req->bindValue(':per_num',$idPersonne,PDO::PARAM_STR);

			$req->execute();

			$avis = $req->fetch(PDO::FETCH_ASSOC);

			$dernierAvis = $avis['dernierAvis'];

			return $dernierAvis;

			$req->closeCursor();
		}
	}

	//Fonction pour supprimer les avis proposé par une  personne
	public function supprimerAvisParPersonne($idPersonne){

		$req = $this->db->prepare('DELETE FROM avis WHERE per_num = :per_num OR per_per_num = :per_num');

		$req->bindValue(':per_num',$idPersonne,PDO::PARAM_STR);

		return $req->execute();

		$req->closeCursor();
	}
}

?>
