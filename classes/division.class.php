<?php
class Division{
	private $div_num;
	private $div_nom;

	//Constructeur de la classe division
	public function __construct($valeurs = array()){
		if(!empty($valeurs)){
			$this->affecte($valeurs);
		}
	}

	//Affectation des donnees a un objet division
	public function affecte($donnees){
		foreach ((array) $donnees as $attribut => $valeur) {
			switch ($attribut) {
				case 'div_num':
					$this->setNumDivision($valeur);
					break;

				case 'div_nom':
						$this->setNomDivision($valeur);
						break;

				default:
					echo "Fatal error : construction division invalide";
					break;
			}
		}
	}

	//Getter de la classe division
	public function getNumDivision(){
		return $this->div_num;
	}

	public function getNomDivision(){
		return $this->div_nom;
	}

	//Setter de la classe division
	public function setNumDivision($nouveau_division_num){
		$this->div_num = $nouveau_division_num;
	}

	public function setNomDivision($nouveau_division_nom){
		$this->div_nom = $nouveau_division_nom;
	}
	
}
