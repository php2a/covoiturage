<?php
class TrajetManager{

	//Conctructeur
	public function __construct($db){
		$this->db = $db;
	}

	//Fonction permettant de créer un objet Ville à partir d'un tableau
	public function creerTrajetDepuisTableau($paramsTrajet){
		return new Trajet($paramsTrajet);
	}

	//Fonction pour récupérer tous les trajet (numero de parcours, numero de personne, date, heure, places, sens)
	public function recupererTousTrajets(){

		$listeTrajets = array();

		$req = $this->db->prepare(
			'SELECT * FROM propose'
		);

		$req->execute();

		while($trajet = $req->fetch(PDO::FETCH_OBJ)){
			$listeTrajets[] = new Trajet($trajet);
		}

		return $listeTrajets;

		$req->closeCursor();
	}


  //Fonction pour récupérer toutes les villes de départ des trajet (numéro, nom)
  public function recupererToutesVillesDepartDesTrajet(){

		$req = $this->db->prepare(
							' SELECT vil_num , vil_nom FROM ville v
								INNER JOIN parcours par ON v.vil_num = par.vil_num1
								INNER JOIN propose pro ON par.par_num = pro.par_num
	              UNION
	              SELECT vil_num , vil_nom FROM ville v
								INNER JOIN parcours par ON v.vil_num = par.vil_num2
								INNER JOIN propose pro ON par.par_num = pro.par_num
	              ORDER BY vil_nom ASC'
		);

		$req->execute();

    $listeVille = array();

    while($ville = $req->fetch(PDO::FETCH_OBJ)){
      $listeVilles[] = new Ville($ville);
    }

    return $listeVilles;

    $req->closeCursor();
  }


	//Fonction pour récupérer toutes les villes d'arrivée possible via un id de ville de départ (numéro, nom)
	public function recupererVillesArriveePossibleViaIdVilleDepart($idVilleDepart){

		$req = $this->db->prepare(
			"	SELECT DISTINCT vil_num, vil_nom FROM propose pro INNER JOIN parcours par ON pro.par_num = par.par_num
				INNER JOIN ville v ON par.vil_num1 = v.vil_num WHERE pro_sens = 0 AND par.par_num
				IN (SELECT pa.par_num FROM parcours pa WHERE vil_num2 = :vil_num)

				UNION

				SELECT DISTINCT vil_num, vil_nom FROM propose pro INNER JOIN parcours par ON pro.par_num = par.par_num
				INNER JOIN ville v ON par.vil_num2 = v.vil_num WHERE pro_sens = 0 AND par.par_num
				IN (SELECT pa.par_num FROM parcours pa WHERE vil_num1 = :vil_num)"
		);

		$req->bindValue(':vil_num',$idVilleDepart,PDO::PARAM_STR);

		$req->execute();

		$listeVillesArriveePossible = array();

		while($ville = $req->fetch(PDO::FETCH_OBJ)){
			$listeVillesArriveePossible[] = new Ville($ville);
		}

		return $listeVillesArriveePossible;

		$req->closeCursor();
	}


	//Fonction pour récupérer toutes les villes d'arrivée possible via un id de ville de départ (numéro, nom)
	public function recupererTrajetPossibleAvecParametres($numParcours, $sensTrajet, $dateDepart, $heureDepart, $precision){

		$req = $this->db->prepare(
			"	SELECT par_num, per_num, pro_date, pro_time, pro_place, pro_sens FROM propose
				WHERE par_num = :par_num AND pro_sens = :pro_sens AND pro_time >= :pro_time AND pro_date
				BETWEEN DATE_SUB(:pro_date,INTERVAL :precision DAY) AND DATE_ADD(:pro_date,INTERVAL :precision DAY) "
		);

		$req->bindValue(':par_num',$numParcours,PDO::PARAM_STR);
		$req->bindValue(':pro_sens',$sensTrajet,PDO::PARAM_STR);
		$req->bindValue(':pro_date',$dateDepart,PDO::PARAM_STR);
		$req->bindValue(':pro_time',$heureDepart,PDO::PARAM_STR);
		$req->bindValue(':precision',$precision-1,PDO::PARAM_STR);

		$req->execute();

		$listeTrajets = array();

		while($trajet = $req->fetch(PDO::FETCH_OBJ)){
			$listeTrajets[] = new Trajet($trajet);
		}

		return $listeTrajets;

		$req->closeCursor();
	}

  //Fonction pour ajouter un nouveau trajet
	public function ajouterTrajet($nouveauTrajet){

		if(!is_null($nouveauTrajet)){
			$req = $this->db->prepare(
				"INSERT INTO propose (par_num, per_num, pro_date, pro_time, pro_place, pro_sens)
        VALUES (:par_num, :per_num, :pro_date, :pro_time, :pro_place, :pro_sens)"
      );

			$req->bindValue(':par_num',$nouveauTrajet->getNumParcoursTrajet(),PDO::PARAM_STR);
			$req->bindValue(':per_num',$nouveauTrajet->getNumPersonneTrajet(),PDO::PARAM_STR);
			$req->bindValue(':pro_date',$nouveauTrajet->getDateTrajet(),PDO::PARAM_STR);
			$req->bindValue(':pro_time',$nouveauTrajet->getTimeTrajet(),PDO::PARAM_STR);
      $req->bindValue(':pro_place',$nouveauTrajet->getPlaceTrajet(),PDO::PARAM_STR);
			$req->bindValue(':pro_sens',$nouveauTrajet->getSensTrajet(),PDO::PARAM_STR);

			return $req->execute();
		}

		$req->closeCursor();
	}

	//Fonction retournant le sens du trajet grâce à son id et à sa ville de départ
	// 0 : ville 1 -> ville 2
	// 1 : ville 2 -> ville 1
	public function getSensTrajet($idParcours, $villeDepart){
		$parcoursManager = new ParcoursManager($this->db);

		$parcours = $parcoursManager->recupererParcoursViaNumero($idParcours);

		if($parcours->getNumVille1Parcours() == $villeDepart){
			return 0;
		} else {
			return 1;
		}
	}


	//Fonction pour supprimer les trajets proposé par une  personne
	public function supprimerTrajetsParPersonne($idPersonne){

		$req = $this->db->prepare('DELETE FROM propose WHERE per_num = :per_num');

		$req->bindValue(':per_num',$idPersonne,PDO::PARAM_STR);

		return $req->execute();

		$req->closeCursor();
	}

}
?>
