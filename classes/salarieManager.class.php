<?php
class SalarieManager{

	//Conctructeur
	public function __construct($db){
		$this->db = $db;
	}

	//Fonction permettant de créer un objet Salarie à partir d'un tableau
	public function creerSalarieDepuisTableau($paramsSalarie){
		return new Salarie($paramsSalarie);
	}

	//Fonction pour récupérer toutes les salaries (numero, tel pro, fonction)
	public function recupererTousSalarie(){

		$listeSalaries = array();

		$req = $this->db->prepare(
			'SELECT * FROM salarie'
		);

		$req->execute();

		while($salarie = $req->fetch(PDO::FETCH_OBJ)){
			$listeSalaries[] = new Salarie($salarie);
		}

		return $listeSalaries;

		$req->closeCursor();
	}

	//Fonction pour récupérer un salarie via son numero (numero, tel pro, fonction)
	public function recupererSalarieViaNumero($idSalarie){

		if(!is_null($idSalarie)){
			$req = $this->db->prepare(
				'SELECT * FROM salarie WHERE per_num = :per_num'
			);

			$req->bindValue(':per_num',$idSalarie,PDO::PARAM_STR);

			$req->execute();

			$salarie = $req->fetch(PDO::FETCH_OBJ);

			return new Salarie($salarie);
		}

		$req->closeCursor();
	}


	//Fonction pour ajouter un nouveau salarie
	public function ajouterSalarie($nouveauSalarie){

		if(!is_null($nouveauSalarie)){
			$req = $this->db->prepare(
				'INSERT INTO salarie (per_num, sal_telprof, fon_num) VALUES (:per_num, :sal_telprof, :fon_num)'
			);

			$req->bindValue(':per_num',$nouveauSalarie->getNumPersonneSalarie(),PDO::PARAM_STR);
			$req->bindValue(':sal_telprof',$nouveauSalarie->getTelProfessionnelSalarie(),PDO::PARAM_STR);
			$req->bindValue(':fon_num',$nouveauSalarie->getFonctionSalarie(),PDO::PARAM_STR);

			return $req->execute();

			$req->closeCursor();
		}
	}

	//Fonction pour mettre a jour un salarie
	public function updateSalarie($nouveauSalarie){

		if(!is_null($nouveauSalarie)){
			$req = $this->db->prepare(
				'UPDATE salarie SET sal_telprof = :sal_telprof, fon_num = :fon_num WHERE per_num = :per_num'
			);

			$req->bindValue(':per_num',$nouveauSalarie->getNumPersonneSalarie(),PDO::PARAM_STR);
			$req->bindValue(':sal_telprof',$nouveauSalarie->getTelProfessionnelSalarie(),PDO::PARAM_STR);
			$req->bindValue(':fon_num',$nouveauSalarie->getFonctionSalarie(),PDO::PARAM_STR);

			return $req->execute();

			$req->closeCursor();
		}
	}


	//Fonction pour supprimer un salarie
	public function supprimerSalarie($idPersonne){

		if(!is_null($idPersonne)){
			$req = $this->db->prepare('DELETE FROM salarie WHERE per_num = :per_num');
		}

		$req->bindValue(':per_num',$idPersonne,PDO::PARAM_STR);

		return $req->execute();

		$req->closeCursor();

	}

}
?>
