<?php
class EtudiantManager{

		//Conctructeur
		public function __construct($db){
			$this->db = $db;
		}

		//Fonction permettant de créer un objet Etudiant à partir d'un tableau
		public function creerEtudiantDepuisTableau($paramsEtudiant){
			return new Etudiant($paramsEtudiant);
		}

		//Fonction pour récupérer toutes les etudiant (numero, departement, division)
		public function recupererTousEtudiants(){

			$listeEtudiants = array();

			$req = $this->db->prepare(
				'SELECT * FROM etudiant'
			);

			$req->execute();

			while($etudiant = $req->fetch(PDO::FETCH_OBJ)){
				$listeEtudiants[] = new Etudiant($etudiant);
			}

			return $listeEtudiants;

			$req->closeCursor();
		}

		//Fonction pour récupérer un etudiant via son numero (numero, departement, division)
		public function recupererEtudiantViaNumero($idEtudiant){

			if(!is_null($idEtudiant)){
				$req = $this->db->prepare(
					'SELECT * FROM etudiant WHERE per_num = :per_num'
				);

				$req->bindValue(':per_num',$idEtudiant,PDO::PARAM_STR);

				$req->execute();

				$etudiant = $req->fetch(PDO::FETCH_OBJ);

				return new Etudiant($etudiant);

				$req->closeCursor();
			}
		}

		//Fonction pour ajouter un nouvel etudiant
		public function ajouterEtudiant($nouvelEtudiant){

			if(!is_null($nouvelEtudiant)){
				$req = $this->db->prepare(
					'INSERT INTO etudiant (per_num, dep_num, div_num) VALUES (:per_num, :dep_num, :div_num)'
				);

				$req->bindValue(':per_num',$nouvelEtudiant->getNumPersonneEtudiant(),PDO::PARAM_STR);
				$req->bindValue(':dep_num',$nouvelEtudiant->getDepartementEtudiant(),PDO::PARAM_STR);
				$req->bindValue(':div_num',$nouvelEtudiant->getDivisionEtudiant(),PDO::PARAM_STR);

				return $req->execute();

				$req->closeCursor();
			}
		}

		//Fonction pour mettre a jour un etudiant
		public function updateEtudiant($nouvelEtudiant){

			if(!is_null($nouvelEtudiant)){
				$req = $this->db->prepare(
					'UPDATE etudiant SET dep_num = :dep_num, div_num = :div_num WHERE per_num = :per_num'
				);

				$req->bindValue(':per_num',$nouvelEtudiant->getNumPersonneEtudiant(),PDO::PARAM_STR);
				$req->bindValue(':dep_num',$nouvelEtudiant->getDepartementEtudiant(),PDO::PARAM_STR);
				$req->bindValue(':div_num',$nouvelEtudiant->getDivisionEtudiant(),PDO::PARAM_STR);

				return $req->execute();

				$req->closeCursor();
			}

		}
		
	//Fonction pour supprimer un etudiant
	public function supprimerEtudiant($idPersonne){

		if(!is_null($idPersonne)){
			$req = $this->db->prepare('DELETE FROM etudiant WHERE per_num = :per_num');
		}

		$req->bindValue(':per_num',$idPersonne,PDO::PARAM_STR);

		return $req->execute();

		$req->closeCursor();

	}

}
?>
