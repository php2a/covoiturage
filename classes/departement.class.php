<?php
class Departement{

	//Déclarations des variables de la classe departement
	private $dep_num;
	private $dep_nom;
	private $vil_num;

	//Constructeur de la classe departement
	public function __construct($valeurs = array()){
		if(!empty($valeurs)){
			$this->affecte($valeurs);
		}
	}

	//Affectation des donnees a un objet departement
	public function affecte($donnees){
		foreach ((array) $donnees as $attribut => $valeur) {
			switch ($attribut) {
				case 'dep_num':
					$this->setNumDepartement($valeur);
					break;

				case 'dep_nom':
						$this->setNomDepartement($valeur);
						break;

				case 'vil_num':
						$this->setVilleNumDepartement($valeur);
						break;

				default:
					echo "Fatal error : construction departement invalide";
					break;
			}
		}
	}

	//Getter de la classe departement
	public function getNumDepartement(){
		return $this->dep_num;
	}

	public function getNomDepartement(){
		return $this->dep_nom;
	}

	public function getVilleNumDepartement(){
		return $this->vil_num;
	}

	//Setter de la classe departement
	public function setNumDepartement($nouveau_departement_num){
		$this->dep_num = $nouveau_departement_num;
	}

	public function setNomDepartement($nouveau_departement_nom){
		$this->dep_nom = $nouveau_departement_nom;
	}

	public function setVilleNumDepartement($nouveau_departement_vil_num){
		$this->vil_num = $nouveau_departement_vil_num;
	}
}
