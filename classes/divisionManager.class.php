<?php
class DivisionManager{

	//Conctructeur
	public function __construct($db){
		$this->db = $db;
	}

	//Fonction pour récupérer toutes les divisons (numero, nom)
	public function recupererToutesDivisions(){

		$listeDivisions = array();

		$req = $this->db->prepare(
			'SELECT * FROM division'
		);

		$req->execute();

		while($division = $req->fetch(PDO::FETCH_OBJ)){
			$listeDivisions[] = new Division($division);
		}

		return $listeDivisions;

		$req->closeCursor();
	}

}

?>
