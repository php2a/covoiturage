<?php
class Avis{

  //Déclarations des variables de la classe avis
  private $per_num;
  private $per_per_num;
  private $par_num;
  private $avis_comm;
  private $avis_note;
  private $avis_date;

  //Constructeur de la classe avis
  public function __construct($valeurs = array()){
    if(!empty($valeurs)){
      $this->affecte($valeurs);
    }
  }

  //Affectation des donnees a un objet avis
  public function affecte($donnees){
    foreach ((array) $donnees as $attribut => $valeur) {
      switch ($attribut) {
        case 'per_num':
          $this->setNumeroPersonneAvis($valeur);
          break;

        case 'per_per_num':
            $this->setNumeroPersonnePersonneAvis($valeur);
            break;

        case 'par_num':
            $this->setNumeroParcoursAvis($valeur);
            break;

        case 'avis_comm':
            $this->setCommentaireAvis($valeur);
            break;

        case 'avis_note':
            $this->setNoteAvis($valeur);
            break;

        case 'avis_date':
            $this->setDateAvis($valeur);
            break;

        default:
          echo "Fatal error : construction departement invalide";
          break;
      }
    }
  }

  //Setter

  public function setNumeroPersonneAvis($nouveau_numero_personne_avis){
    $this->per_num = $nouveau_numero_personne_avis;
  }

  public function setNumeroPersonnePersonneAvis($nouveau_numero_personne_personne_avis){
    $this->per_per_num = $nouveau_numero_personne_personne_avis;
  }

  public function setNumeroParcoursAvis($nouveau_numero_parcours_avis){
    $this->par_num = $nouveau_numero_parcours_avis;
  }

  public function setCommentaireAvis($nouveau_commentaire_avis){
    $this->avis_comm = $nouveau_commentaire_avis;
  }

  public function setNoteAvis($nouveau_note_avis){
    $this->avis_note = $nouveau_note_avis;
  }

  public function setDateAvis($nouveau_date_avis){
    $this->avis_date = $nouveau_date_avis;
  }


  //Getter

  public function getNumeroPersonneAvis(){
    return $this->per_num;
  }

  public function getNumeroPersonnePersonneAvis(){
    return $this->per_per_num;
  }

  public function getNumeroParcoursAvis(){
    return $this->par_num;
  }

  public function getCommentaireAvis(){
    return $this->avis_comm;
  }

  public function getNoteAvis(){
    return $this->avis_note;
  }

  public function getDateAvis(){
    return $this->avis_date;
  }

}
?>
