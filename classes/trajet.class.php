<?php
class Trajet{

	//Déclarations des variables de la classe parcours
	private $par_num;
	private $per_num;
	private $pro_date;
	private $pro_time;
	private $pro_place;
	private $pro_sens;


	//Constructeur de la classe parcours
	public function __construct($valeurs = array()){
		if(!empty($valeurs)){
			$this->affecte($valeurs);
		}
	}

	//Affectation des donnees a un objet parcours
	public function affecte($donnees){

		foreach ((array) $donnees as $attribut => $valeur) {
			switch ($attribut) {
				case 'par_num':
					$this->setNumParcoursTrajet($valeur);
					break;

				case 'per_num':
					$this->setNumPersonneTrajet($valeur);
					break;

				case 'pro_date':
					$this->setDateTrajet($valeur);
					break;

				case 'pro_time':
					$this->setTimeTrajet($valeur);
					break;

				case 'pro_place':
					$this->setPlaceTrajet($valeur);
				break;

				case 'pro_sens':
					$this->setSensTrajet($valeur);
				break;

				default:
					echo "Fatal error : construction trajet invalide";
					break;
			}
		}
	}

	//Getter de la classe trajet
	public function getNumParcoursTrajet(){
		return $this->par_num;
	}

	public function getNumPersonneTrajet(){
		return $this->per_num;
	}

	public function getDateTrajet(){
		return $this->pro_date;
	}

	public function getTimeTrajet(){
		return $this->pro_time;
	}

	public function getPlaceTrajet(){
		return $this->pro_place;
	}

	public function getSensTrajet(){
		return $this->pro_sens;
	}

	//Setter de la classe trajet
	public function setNumParcoursTrajet($nouveau_parcours_num_traj){
		$this->par_num = $nouveau_parcours_num_traj;
	}

	public function setNumPersonneTrajet($nouveau_num_pers_traj){
		$this->per_num = $nouveau_num_pers_traj;
	}

	public function setDateTrajet($nouveau_date_traj){
		$this->pro_date = $nouveau_date_traj;
	}

	public function setTimeTrajet($nouveau_time_traj){
		$this->pro_time = $nouveau_time_traj;
	}

	public function setPlaceTrajet($nouveau_place_traj){
		$this->pro_place = $nouveau_place_traj;
	}

	public function setSensTrajet($nouveau_sens_trajet){
		$this->pro_sens = $nouveau_sens_trajet;
	}

}
