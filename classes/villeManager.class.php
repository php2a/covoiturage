<?php
class VilleManager{

	//Conctructeur
	public function __construct($db){
		$this->db = $db;
	}

	//Fonction permettant de créer un objet Ville à partir d'un tableau
	public function creerVilleDepuisTableau($paramsVille){
		return new Ville($paramsVille);
	}

	//Fonction pour récupérer toutes les villes (numéro et nom)
	public function recupererToutesVilles(){

		$listeVilles = array();

		$req = $this->db->prepare(
			'SELECT * FROM ville ORDER BY vil_nom ASC '
		);

		$req->execute();

		while($ville = $req->fetch(PDO::FETCH_OBJ)){
			$listeVilles[] = new Ville($ville);
		}

		return $listeVilles;

		$req->closeCursor();
	}

	//Fonction pour récupérer une ville grace a son numéro
	public function recupererVilleParNum($numVille){

		$req = $this->db->prepare(
			'SELECT * FROM ville WHERE vil_num = :vil_num'
		);

		$req->bindValue(':vil_num',$numVille,PDO::PARAM_STR);

		$req->execute();

		$ville = $req->fetch(PDO::FETCH_OBJ);

		return new Ville($ville);

		$req->closeCursor();
	}

	//Fonction permettant de recupere le nom d'une ville à partir de son identifiant
	public function recupererNomVilleParNum($numVille){
		return $this->recupererVilleParNum($numVille)->getNomVille();
	}

	//Fonction pour ajouter une nouvelle ville
	public function ajouterVille($nouvelleVille){

		if(!is_null($nouvelleVille)){

			$req = $this->db->prepare(
				'INSERT INTO ville (vil_nom) VALUES (:vil_nom)'
			);

			$req->bindValue(':vil_nom',$nouvelleVille->getNomVille(),PDO::PARAM_STR);

			return $req->execute();

			$req->closeCursor();
		}
	}

	//Fonction permettant de verifier que la ville a ajouter n'existe pas deja
	public function verifierSiVilleExisteDeja($nouvelleVille){

		if(!is_null($nouvelleVille)){
			$req = $this->db->prepare(
				'SELECT vil_nom FROM ville WHERE UPPER(vil_nom) = :vil_nom'
			);

			$req->bindValue(':vil_nom',strtoupper($nouvelleVille->getNomVille()),PDO::PARAM_STR);

			$req->execute();

			return !$req->fetch(PDO::FETCH_ASSOC);

			$req->closeCursor();
		}

	}

	//Fonction permettant de récupérer un objet Ville via son identifiant
	public function recupererVilleNumViaNumDepartement($idDepartement){

		if(!is_null($idDepartement)){
			$req = $this->db->prepare(
				'SELECT vil_num FROM departement WHERE dep_num = :dep_num'
			);

			$req->bindValue(':dep_num',$idDepartement,PDO::PARAM_STR);

			$req->execute();

			$numVille = $req->fetch(PDO::FETCH_ASSOC);

			$req->closeCursor();

			return $numVille['vil_num'];
		}

	}

}

?>
