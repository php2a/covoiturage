<?php
class ParcoursManager{

	//Conctructeur
	public function __construct($db){
		$this->db = $db;
	}

	//Fonction permettant de créer un objet Parcours à partir d'un tableau
	public function creerParcoursDepuisTableau($paramsParcours){
		return new Parcours($paramsParcours);
	}

	//Fonction pour récupérer tous les parcours (ville 1, ville 2 et nombre de kilomètres)
	public function recupererTousParcours(){

		$listeParcours = array();

		$req = $this->db->prepare(
			'SELECT * FROM parcours'
		);

		$req->execute();

		while($parcours = $req->fetch(PDO::FETCH_OBJ)){
			$listeParcours[] = new Parcours($parcours);
		}

		return $listeParcours;

		$req->closeCursor();
	}

	//Fonction pour récupérer un parcours via son id (ville 1, ville 2 et nombre de kilomètres)
	public function recupererParcoursViaNumero($idParcours){

		if(!is_null($idParcours)){
			$req = $this->db->prepare(
				'SELECT * FROM parcours WHERE par_num = :par_num'
			);

			$req->bindValue(':par_num',$idParcours,PDO::PARAM_STR);

			$req->execute();

			$parcours = $req->fetch(PDO::FETCH_OBJ);

			return new Parcours($parcours);

			$req->closeCursor();
		}
	}

	//Fonction pour récupérer toutes les villes (numéro, nom)
	public function recupererToutesVillesDesParcours(){

		$listeVilles = array();

		$req = $this->db->prepare(
							' SELECT vil_num, vil_nom FROM ville v INNER JOIN parcours p ON v.vil_num=p.vil_num1
								UNION
								SELECT vil_num, vil_nom FROM ville v INNER JOIN parcours p ON v.vil_num=p.vil_num2
								ORDER BY vil_nom ASC'
		);

		$req->execute();

		while($ville = $req->fetch(PDO::FETCH_OBJ)){
			$listeVilles[] = new Ville($ville);
		}

		return $listeVilles;

		$req->closeCursor();
	}


	//Fonction pour récupérer toutes les ville de départ via le numero de parcours
	public function recupererVillesDepartArriveeParcours($departOuArrivee, $idParcours){

		$req = $this->db->prepare(
			'SELECT vil_num, vil_nom FROM ville v INNER JOIN parcours p ON v.vil_num=p.vil_num1'
		);

		$req->execute();

		while($ville = $req->fetch(PDO::FETCH_OBJ)){
			$listeVilles[] = new Ville($ville);
		}

		return $listeVilles;

		$req->closeCursor();
	}

	//Fonction pour récupérer toutes les villes d'arrivée possible via un id de ville de départ (numéro, nom)
	public function recupererVillesArriveePossibleViaIdVilleDepart($idVilleDepart){

		$req = $this->db->prepare(
			"	SELECT DISTINCT vil_num, vil_nom FROM propose pro INNER JOIN parcours par ON pro.par_num = par.par_num
				INNER JOIN ville v ON par.vil_num1 = v.vil_num WHERE vil_num2 = :vil_num

				UNION

				SELECT DISTINCT vil_num, vil_nom FROM propose pro INNER JOIN parcours par ON pro.par_num = par.par_num
				INNER JOIN ville v ON par.vil_num2 = v.vil_num WHERE vil_num1 = :vil_num"
		);

		$req->bindValue(':vil_num',$idVilleDepart,PDO::PARAM_STR);

		$req->execute();

		$listeVillesArriveePossible = array();

		while($ville = $req->fetch(PDO::FETCH_OBJ)){
			$listeVillesArriveePossible[] = new Ville($ville);
		}

		return $listeVillesArriveePossible;

		$req->closeCursor();
	}

	//Fonction permettant d'insérer un nouveau Parcours dans la base de donnée
	public function ajouterParcours($nouveauParcours){

		if(!is_null($nouveauParcours)){
			$req = $this->db->prepare(
				'INSERT INTO parcours (par_km, vil_num1, vil_num2) VALUES(:par_km, :vil_num1, :vil_num2)'
			);

			$req->bindValue(':par_km',(float)$nouveauParcours->getKmParcours(),PDO::PARAM_STR);
			$req->bindValue(':vil_num1',$nouveauParcours->getNumVille1Parcours(),PDO::PARAM_STR);
			$req->bindValue(':vil_num2',$nouveauParcours->getNumVille2Parcours(),PDO::PARAM_STR);

			return $req->execute();

			$req->closeCursor();
		}

	}

	public function parcoursExisteDeja($nouveauParcours){

		if(!is_null($nouveauParcours)){
			$req = $this->db->prepare(
				"SELECT * FROM parcours WHERE (vil_num1 = :vil_depart AND vil_num2 = :vil_arrivee) OR (vil_num2 = :vil_depart AND vil_num1 = :vil_arrivee)"
			);

			$req->bindValue(':vil_depart',$nouveauParcours->getNumVille1Parcours(),PDO::PARAM_STR);
			$req->bindValue(':vil_arrivee',$nouveauParcours->getNumVille2Parcours(),PDO::PARAM_STR);

			$req->execute();

		 	$parcours = $req->fetch(PDO::FETCH_OBJ);

			if($parcours){
				return new Parcours($parcours);
			} else {
				return NULL;
			}

			$req->closeCursor();
		}
	}

}
?>
