<?php
class FonctionManager{
	//Conctructeur
	public function __construct($db){
		$this->db = $db;
	}

	//Fonction pour récupérer toutes les fonctions (numero, libellé)
	public function recupererToutesFonctions(){

		$listeFonctions = array();

		$req = $this->db->prepare(
			'SELECT * FROM fonction'
		);

		$req->execute();

		while($fonction = $req->fetch(PDO::FETCH_OBJ)){
			$listeFonctions[] = new Fonction($fonction);
		}

		return $listeFonctions;

		$req->closeCursor();
	}

	//Fonction retournant un objet Salarie via son identifiant
	public function recupererFonctionSalarieViaNumero($idFonction){
		if(!is_null($idFonction)){
			$req = $this->db->prepare(
				'SELECT * FROM fonction WHERE fon_num = :fon_num'
			);

			$req->bindValue(':fon_num',$idFonction,PDO::PARAM_STR);

			$req->execute();

			$fonction = $req->fetch(PDO::FETCH_OBJ);

			return new Fonction($fonction);
		}

		$req->closeCursor();
	}

}
?>
