<?php
class PersonneManager{

	private $lastInsertId;

	//Conctructeur
	public function __construct($db){
		$this->db = $db;
		$this->lastInsertId = 0;
	}

	//Fonction permettant de créer un objet Personne à partir d'un tableau
	public function creerPersonneDepuisTableau($paramsPersonne){
		return new Personne($paramsPersonne);
	}

	//Fonction pour récupérer toutes les personnes (numero, nom, prénom, tel perso, mail, login, mdp)
	public function recupererToutesPersonnes(){

		$listePersonnes = array();

		$req = $this->db->prepare(
			'SELECT * FROM personne'
		);

		$req->execute();

		while($personne = $req->fetch(PDO::FETCH_OBJ)){
			$listePersonnes[] = new Personne($personne);
		}

		return $listePersonnes;

		$req->closeCursor();
	}

	//Fonction pour récupérer une personne via son id (numero, nom, prénom, tel perso, mail, login, mdp)
	public function recupererPersonneParId($idPersonne){

		$req = $this->db->prepare(
			"SELECT * FROM personne WHERE per_num = :per_num"
		);

		$req->bindValue(':per_num',$idPersonne,PDO::PARAM_STR);

		$req->execute();

		$personne = $req->fetch(PDO::FETCH_OBJ);

		return new Personne($personne);

		$req->closeCursor();
	}

	//Fonction pour ajouter une nouvelle personne
	public function ajouterPersonne($nouvellePersonne){
		if(!is_null($nouvellePersonne)){

			$req = $this->db->prepare(
				'	INSERT INTO personne (per_nom, per_prenom, per_tel, per_mail, per_login, per_pwd)
					VALUES (:per_nom, :per_prenom, :per_tel, :per_mail, :per_login, :per_pwd)'
			);

			$req->bindValue(':per_nom',$nouvellePersonne->getNomPersonne(),PDO::PARAM_STR);
			$req->bindValue(':per_prenom',$nouvellePersonne->getPrenomPersonne(),PDO::PARAM_STR);
			$req->bindValue(':per_tel',$nouvellePersonne->getTelPersonne(),PDO::PARAM_STR);
			$req->bindValue(':per_mail',$nouvellePersonne->getMailPersonne(),PDO::PARAM_STR);
			$req->bindValue(':per_login',$nouvellePersonne->getLoginPersonne(),PDO::PARAM_STR);
			$req->bindValue(':per_pwd',$nouvellePersonne->getPwdPersonne(),PDO::PARAM_STR);

			$result = $req->execute();

			$this->lastInsertId = $this->db->lastInsertId();
			$_SESSION['idPersonneEnCreation'] = $this->lastInsertId;

			$req->closeCursor();

			return $result;
		}

	}

	//Fonction pour mettre a jour une personne
	public function updatePersonne($nouvellePersonne){
		if(!is_null($nouvellePersonne)){

			$req = $this->db->prepare(
				'	UPDATE personne SET per_nom = :per_nom, per_prenom = :per_prenom, per_tel = :per_tel, per_mail = :per_mail, per_login = :per_login, per_pwd = :per_pwd
					WHERE per_num = :per_num'
			);

			$req->bindValue(':per_nom',$nouvellePersonne->getNomPersonne(),PDO::PARAM_STR);
			$req->bindValue(':per_prenom',$nouvellePersonne->getPrenomPersonne(),PDO::PARAM_STR);
			$req->bindValue(':per_tel',$nouvellePersonne->getTelPersonne(),PDO::PARAM_STR);
			$req->bindValue(':per_mail',$nouvellePersonne->getMailPersonne(),PDO::PARAM_STR);
			$req->bindValue(':per_login',$nouvellePersonne->getLoginPersonne(),PDO::PARAM_STR);
			$req->bindValue(':per_pwd',$nouvellePersonne->getPwdPersonne(),PDO::PARAM_STR);
			$req->bindValue(':per_num',$nouvellePersonne->getNumPersonne(),PDO::PARAM_STR);

			$res = $req->execute();

			$_SESSION['idPersonneEnCreation'] = $nouvellePersonne->getNumPersonne();

			$req->closeCursor();

			return $res;
		}

	}

	//Fonction pour supprimer une  personne
	public function supprimerPersonne($idPersonne){

		if(!is_null($idPersonne)){

			if($this->estEtudiant($idPersonne)){
				$etudiantManager = new EtudiantManager($this->db);
				$supprimer_type_personne = $etudiantManager->supprimerEtudiant($idPersonne);
			} else {
				$salarieManager = new SalarieManager($this->db);
				$supprimer_type_personne = $salarieManager->supprimerSalarie($idPersonne);
			}

			$trajetManager = new TrajetManager($this->db);
			$supprimer_trajet_personne = $trajetManager->supprimerTrajetsParPersonne($idPersonne);

			$avisManager = new AvisManager($this->db);
			$supprimer_avis_personne =$avisManager->supprimerAvisParPersonne($idPersonne);

			$req = $this->db->prepare(
				'DELETE FROM personne WHERE per_num = :per_num'
			);

			$req->bindValue(':per_num',$idPersonne,PDO::PARAM_STR);

			return $req->execute() && $supprimer_type_personne && $supprimer_trajet_personne && $supprimer_avis_personne;

			$req->closeCursor();
		}

	}

	//Fonction permettant de récupérer le dernier identifiant insérer dans la table Personne
	public function lastInsertId(){
		return $this->lastInsertId;
	}

	//FOnction retournant un un boolean indiquant si la personne est un etudiant ou non à partir de son identifiant
	public function estEtudiant($idPersonne){
		if(!is_null($idPersonne)){

			$req = $this->db->prepare('SELECT per_num FROM etudiant WHERE per_num = :per_num');

			$req->bindValue(':per_num',$idPersonne,PDO::PARAM_STR);

		 	$req->execute();

			return $req->fetch(PDO::FETCH_OBJ);

			$req->closeCursor();
		} else {
			return FALSE;
		}
	}

}
?>
