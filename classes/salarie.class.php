<?php
class Salarie{

	private $per_num;
	private $sal_telprof;
	private $fon_num;

	//Constructeur de la classe division
	public function __construct($valeurs = array()){
		if(!empty($valeurs)){
			$this->affecte($valeurs);
		}
	}

	//Affectation des donnees a un objet etudiant
	public function affecte($donnees){
		foreach ((array) $donnees as $attribut => $valeur) {
			switch ($attribut) {
				case 'per_num':
					$this->setNumPersonneSalarie($valeur);
					break;

				case 'sal_telprof':
						$this->setTelProfessionnelSalarie($valeur);
						break;

				case 'fon_num':
						$this->setFonctionSalarie($valeur);
						break;

				default:
					echo "Fatal error : construction division invalide";
					break;
			}
		}
	}

	//Getter de la classe etudiant
	public function getNumPersonneSalarie(){
		return $this->per_num;
	}

	public function getTelProfessionnelSalarie(){
		return $this->sal_telprof;
	}

	public function getFonctionSalarie(){
		return $this->fon_num;
	}

	//Setter de la classe etudiant
	public function setNumPersonneSalarie($nouveau_personne_salarie_num){
		$this->per_num = $nouveau_personne_salarie_num;
	}

	public function setTelProfessionnelSalarie($nouveau_salarie_tel_prof){
		$this->sal_telprof = $nouveau_salarie_tel_prof;
	}

	public function setFonctionSalarie($nouveau_salarie_fonction){
		$this->fon_num = $nouveau_salarie_fonction;
	}
}
?>
