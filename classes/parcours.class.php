<?php
class Parcours{

	//Déclarations des variables de la classe parcours
	private $par_num;
	private $par_km;
	private $vil_num1;
	private $vil_num2;

	//Constructeur de la classe parcours
	public function __construct($valeurs = array()){
		if(!empty($valeurs)){
			$this->affecte($valeurs);
		}
	}

	//Affectation des donnees a un objet parcours
	public function affecte($donnees){
		foreach ((array) $donnees as $attribut => $valeur) {
			switch ($attribut) {
				case 'par_num':
					$this->setNumParcours($valeur);
					break;

				case 'par_km':
					$this->setKmParcours($valeur);
					break;

				case 'vil_num1':
					$this->setVille1Parcours($valeur);
					break;

				case 'vil_num2':
					$this->setVille2Parcours($valeur);
					break;

				default:
					echo "Fatal error : construction parcours invalide";
					break;
			}
		}
	}

	//Getter de la classe parcours
	public function getNumParcours(){
		return $this->par_num;
	}

	public function getKmParcours(){
		return $this->par_km;
	}

	public function getNumVille1Parcours(){
		return $this->vil_num1;
	}

	public function getNumVille2Parcours(){
		return $this->vil_num2;
	}

	//Setter de la classe parcours
	public function setNumParcours($nouveau_par_num){
		$this->par_num = $nouveau_par_num;
	}

	public function setKmParcours($nouveau_par_km){
		$this->par_km = $nouveau_par_km;
	}

	public function setVille1Parcours($nouveau_vil_num1){
		$this->vil_num1 = $nouveau_vil_num1;
	}

	public function setVille2Parcours($nouveau_vil_num2){
		$this->vil_num2 = $nouveau_vil_num2;
	}


}
