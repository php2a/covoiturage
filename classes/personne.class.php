<?php
class Personne{
	private $per_num;
	private $per_nom;
	private $per_prenom;
	private $per_tel;
	private $per_mail;
	private $per_login;
	private $per_pwd;

	//Constructeur de la classe personne
	public function __construct($valeurs = array()){
		if(!empty($valeurs)){
			$this->affecte($valeurs);
		}
	}

	//Affectation des donnees a un objet personne
	public function affecte($donnees){
		foreach ((array) $donnees as $attribut => $valeur) {
			switch ($attribut) {
				case 'per_num':
					$this->setNumPersonne($valeur);
					break;

				case 'per_nom':
					$this->setNomPersonne($valeur);
					break;

				case 'per_prenom':
					$this->setPrenomPersonne($valeur);
					break;

				case 'per_tel':
					$this->setTelPersonne($valeur);
					break;

				case 'per_mail':
					$this->setMailPersonne($valeur);
					break;

				case 'per_login':
					$this->setLoginPersonne($valeur);
					break;

				case 'per_pwd':
					$this->setPwdPersonne($valeur);
					break;

				default:
					echo "Fatal error : construction division invalide";
					break;
			}
		}
	}

	//Getter de la classe division
	public function getNumPersonne(){
		return $this->per_num;
	}

	public function getNomPersonne(){
		return $this->per_nom;
	}

	public function getPrenomPersonne(){
		return $this->per_prenom;
	}

	public function getTelPersonne(){
		return $this->per_tel;
	}

	public function getMailPersonne(){
		return $this->per_mail;
	}

	public function getLoginPersonne(){
		return $this->per_login;
	}

	public function getPwdPersonne(){
		return $this->per_pwd;
	}

	public function getNomPrenomPersonne(){
		return $this->getPrenomPersonne()." ".$this->getNomPersonne();
	}

	//Setter de la classe division
	public function setNumPersonne($nouvelle_personne_num){
		$this->per_num = $nouvelle_personne_num;
	}

	public function setNomPersonne($nouvelle_personne_nom){
		$this->per_nom = $nouvelle_personne_nom;
	}

	public function setPrenomPersonne($nouvelle_personne_prenom){
		$this->per_prenom = $nouvelle_personne_prenom;
	}

	public function setTelPersonne($nouvelle_personne_tel){
		$this->per_tel = $nouvelle_personne_tel;
	}

	public function setMailPersonne($nouvelle_personne_mail){
		$this->per_mail = $nouvelle_personne_mail;
	}

	public function setLoginPersonne($nouvelle_personne_login){
		$this->per_login= $nouvelle_personne_login;
	}

	public function setPwdPersonne($nouvelle_personne_pwd){
		$this->per_pwd = $nouvelle_personne_pwd;
	}
}
