<?php
class Fonction{
	private $fon_num;
	private $fon_libelle;

	//Constructeur de la classe Fonction
	public function __construct($valeurs = array()){
		if(!empty($valeurs)){
			$this->affecte($valeurs);
		}
	}

	//Affectation des donnees a un objet Fonction
	public function affecte($donnees){
		foreach ((array) $donnees as $attribut => $valeur) {
			switch ($attribut) {
				case 'fon_num':
					$this->setNumFonction($valeur);
					break;

				case 'fon_libelle':
						$this->setLibelleFonction($valeur);
						break;

				default:
					echo "Fatal error : construction division invalide";
					break;
			}
		}
	}

	//Getter de la classe etudiant
	public function getNumFonction(){
		return $this->fon_num;
	}

	public function getLibelleFonction(){
		return $this->fon_libelle;
	}

	//Setter de la classe etudiant
	public function setNumFonction($nouveau_fonction_num){
		$this->fon_num = $nouveau_fonction_num;
	}

	public function setLibelleFonction($nouveau_fonction_libelle){
		$this->fon_libelle = $nouveau_fonction_libelle;
	}

}
?>
