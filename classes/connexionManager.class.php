<?php
class ConnexionManager{

  //Conctructeur
	public function __construct($db){
		$this->db = $db;
	}

	//Fonction permettant d'encrypter un mot de passe selon le model choisi
  public function encryptPwd($pwd){
    return sha1(sha1($pwd).SALT);
  }

	//Fonction retournant un objet personne si les identifiants sont correct sinon un NULL
  public function tentativeConnexion($login, $pwd){
    if(!is_null($login) && !is_null($pwd)){
			$req = $this->db->prepare(
				"SELECT * FROM personne WHERE per_login = :per_login AND per_pwd = :per_pwd"
      );

			$req->bindValue(':per_login',$login,PDO::PARAM_STR);
		  $req->bindValue(':per_pwd',$this->encryptPwd($pwd),PDO::PARAM_STR);

			$req->execute();

      $personne = $req->fetch(PDO::FETCH_OBJ);

			if($personne){
				return new Personne($personne);
			} else {
				return NULL;
			}

			$req->closeCursor();
		}
  }
}
?>
