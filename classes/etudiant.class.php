<?php
class Etudiant{

	private $per_num;
	private $div_num;
	private $dep_num;

	//Constructeur de la classe division
	public function __construct($valeurs = array()){
		if(!empty($valeurs)){
			$this->affecte($valeurs);
		}
	}

	//Affectation des donnees a un objet etudiant
	public function affecte($donnees){
		foreach ((array) $donnees as $attribut => $valeur) {
			switch ($attribut) {
				case 'per_num':
					$this->setNumPersonneEtudiant($valeur);
					break;

				case 'dep_num':
						$this->setDepartementEtudiant($valeur);
						break;

				case 'div_num':
						$this->setDivisionEtudiant($valeur);
						break;

				default:
					echo "Fatal error : construction division invalide";
					break;
			}
		}
	}

	//Getter de la classe etudiant
	public function getNumPersonneEtudiant(){
		return $this->per_num;
	}

	public function getDepartementEtudiant(){
		return $this->dep_num;
	}

	public function getDivisionEtudiant(){
		return $this->div_num;
	}

	//Setter de la classe etudiant
	public function setNumPersonneEtudiant($nouveau_personne_etudiant_num){
		$this->per_num = $nouveau_personne_etudiant_num;
	}

	public function setDepartementEtudiant($nouveau_departement_num){
		$this->dep_num = $nouveau_departement_num;
	}

	public function setDivisionEtudiant($nouveau_division_num){
		$this->div_num = $nouveau_division_num;
	}

}
?>
