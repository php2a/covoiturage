<?php
class Ville{
	//Déclarations des variables de la classe ville
	private $vil_num;
	private $vil_nom;

	//Constructeur de la classe ville
	public function __construct($valeurs = array()){
		if(!empty($valeurs)){
			$this->affecte($valeurs);
		}
	}

	//Affectation des donnees a un objet ville
	public function affecte($donnees){
		foreach ((array) $donnees as $attribut => $valeur) {
			switch ($attribut) {
				case 'vil_num':
					$this->setNumVille($valeur);
					break;

				case 'vil_nom':
						$this->setNomVille($valeur);
						break;

				default:
					echo "Fatal error : construction ville invalide";
					break;
			}
		}
	}

	//Getter de la classe ville
	public function getNumVille(){
		return $this->vil_num;
	}

	public function getNomVille(){
		return $this->vil_nom;
	}

	//Setter de la classe ville
	public function setNumVille($nouveau_vil_num){
		$this->vil_num = $nouveau_vil_num;
	}

	public function setNomVille($nouveau_vil_nom){
		$this->vil_nom = $nouveau_vil_nom;
	}

}
?>
