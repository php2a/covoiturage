<?php
class DepartementManager{

	//Conctructeur
	public function __construct($db){
		$this->db = $db;
	}

	//Fonction pour récupérer tous les département (numero, nom et numero ville)
	public function recupererTousDepartement(){

		$listeDepartements = array();

		$req = $this->db->prepare(
			'SELECT * FROM departement'
		);

		$req->execute();

		while($departement = $req->fetch(PDO::FETCH_OBJ)){
			$listeDepartements[] = new Departement($departement);
		}

		return $listeDepartements;

		$req->closeCursor();
	}

}
?>
