//Fonstion permettant de verifier la conformite du formulaire d'ajout de parcours
function validateFormulaireParcours() {
  var selectVille1Parcours = document.getElementById("ville1Parcours");
  var selectVille2Parcours = document.getElementById("ville2Parcours");
  var ville1Parcours;
  var ville2Parcours;

  ville1Parcours = selectVille1Parcours.value;
  ville2Parcours = selectVille2Parcours.value;

  if(ville1Parcours == ville2Parcours){
    alert("Un trajet vers la même ville ... T'est pas très utils toi");
    return false;
  } else {
    return true;
  }
}

//Fonction permettant de verifier le bon formatage d'un numero de téléphone
function verifTel(champ){
  if(champ.value.length != 10){
    surligne(champ, true);
    return false;
  } else {
    surligne(champ, false);
    return true;
  }
}

//Fonction appliquant un sytle au champ mal rensaigné
function surligne(champ, erreur){
  if(erreur){
    champ.style.borderColor  = "red";
  } else {
    champ.style.borderColor  = "";
  }
}

//Fonction permettant de verifier la conformité du formulaire d'ajout/modification de personne
function verifFormulaireAjoutPersonne(form){
  if(verifTel(form.telNouvellePersonne)){
    return true;
  } else {
    alert("Veuillez remplir correctement tous les champs");
    return false;
  }

}
