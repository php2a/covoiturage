<?php
session_start();
//164.81.120.4
//user12
//mdp : 41219
//port : 20012

require('include/config.inc.php');
require('include/autoLoad.inc.php');

$db = new MyPDO;
$avisManager = new AvisManager($db);
$connexionManager = new ConnexionManager($db);
$departementManager = new DepartementManager($db);
$divisionManager = new DivisionManager($db);
$etudiantManager = new EtudiantManager($db);
$fonctionManager = new FonctionManager($db);
$parcoursManager = new ParcoursManager($db);
$personneManager = new PersonneManager($db);
$salarieManager = new SalarieManager($db);
$trajetManager = new TrajetManager($db);
$villeManager = new VilleManager($db);

?>
<script type="text/javascript" src="./js/eventHandler.js"></script>
<?php

require_once("include/header.inc.php");

?>
<div id="corps">
<?php
require_once("include/menu.inc.php");

require_once("include/texte.inc.php");
?>
</div>

<div id="spacer"></div>
<?php
require_once("include/footer.inc.php"); ?>
